#lang racket

(require brag/support)
(require br-parser-tools/lex)
(require syntax/parse)
(require (prefix-in : br-parser-tools/lex-sre))
(require "thebnf.rkt")
(require "tokenize.rkt")
(require "interpret.rkt")
(require racket/cmdline)

(define file-to-interpret
  (command-line
   #:program "interpret"
   #:args (filename) ; expect one command-line argument: <filename>
   ; return the argument as a filename to compile
   filename))

; Open a string as a file
(define file-input (open-input-file file-to-interpret))
; Run the token program from above on our input
(define token-thunk (tokenize file-input))
; Run the parse function
(define program-stx (parse token-thunk))

(interpret-program program-stx)
