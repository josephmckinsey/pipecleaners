# Pipecleaners

Dataflow programming language implemented in racket.

## Dev. Environment

You'll need to install the packages `brag` and `syntax/parse` to run `pipecleaners`:
i.e.
```
raco pkg install brag
```

## Running

```racket pipcleaner.rkt filename.pc```
